package com.bovkun.service;

import com.bovkun.controller.dto.Roles;
import com.bovkun.database.entities.Role;
import com.bovkun.database.entities.Setting;
import com.bovkun.database.entities.User;
import com.bovkun.database.entities.VehicleType;
import com.bovkun.database.repository.*;
import com.bovkun.service.interfaces.AdminService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdminServiceImpl implements AdminService {

    private final SettingsRepository settingsRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    public AdminServiceImpl(SettingsRepository settingsRepository, UserRepository userRepository, RoleRepository roleRepository, VehicleTypeRepository vehicleTypeRepository) {
        this.settingsRepository = settingsRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.vehicleTypeRepository = vehicleTypeRepository;
    }

    /**
     * Updates setting value
     * @param setting
     * @return
     */
    private Setting updateSetting(Setting setting){
        return settingsRepository.save(setting);
    }

    @Override
    public Setting updatePerMilePrice(Double value) throws NotFoundException {
        Optional<Setting> settingOptional = settingsRepository.findByKey(Constants.PER_MILE_PRICE_SETTING);
        if (!settingOptional.isPresent())
            throw new NotFoundException("Setting " + Constants.PER_MILE_PRICE_SETTING + " was not found.");
        Setting setting = settingOptional.get();
        String val = value.toString();
        setting.setValue(val);
        return updateSetting(setting);
    }

    @Override
    public Setting updatePerMinutePrice(Double value) throws NotFoundException {
        Optional<Setting> settingOptional = settingsRepository.findByKey(Constants.PER_MINUTE_PRICE_SETTING);
        if (!settingOptional.isPresent())
            throw new NotFoundException("Setting " + Constants.PER_MINUTE_PRICE_SETTING + " was not found.");
        Setting setting = settingOptional.get();
        String val = value.toString();
        setting.setValue(val);
        return updateSetting(setting);
    }

    @Override
    public User createAdmin(Long userId) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);
        if (!userOptional.isPresent())
            throw new NotFoundException("User with id " + userId + " not found.");
        User user = userOptional.get();
        Role adminRole = roleRepository.findOneById(Roles.ADMIN.getId());
        user.getRoles().add(adminRole);
        return userRepository.save(user);
    }

    @Override
    public User updateBalance(Long userId, Double balance) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(userId);
        if (!userOptional.isPresent())
            throw new NotFoundException("User with id " + userId + " not found.");
        User user = userOptional.get();
        user.setBalance(balance);
        return userRepository.save(user);
    }

    @Override
    public VehicleType updateVehicleType(Integer type, Double priceMultiplier) throws NotFoundException {
        Optional<VehicleType> inDb = vehicleTypeRepository.findById(type);
        if (!inDb.isPresent())
            throw new NotFoundException("VehicleType with type id " + type + " was not found.");
        VehicleType currentType = inDb.get();
        currentType.setPriceMultiplier(priceMultiplier);
        vehicleTypeRepository.save(currentType);
        return currentType;
    }


}

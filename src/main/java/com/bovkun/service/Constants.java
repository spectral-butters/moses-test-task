package com.bovkun.service;

public interface Constants {
    String PER_MILE_PRICE_SETTING = "perMilePrice";
    String PER_MINUTE_PRICE_SETTING = "perMinutePrice";
    String TIME_DELAY_SETTING = "timeDelay";
    Long DEFAULT_TIME_DELAY = 10L;
    Long DEFAULT_DISTANCE_TO_MERGE = 500L;
}

package com.bovkun.service;

import com.bovkun.controller.dto.google.DirectionsRequest;
import com.bovkun.controller.dto.google.EstimatedRoute;
import com.bovkun.database.entities.Ride;
import com.bovkun.exception.RequestedRoutesNotFound;
import com.bovkun.service.interfaces.GoogleMapsService;
import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Arrays;

@Service
public class GoogleMapsServiceImpl implements GoogleMapsService {

    @Value("${google.api.key}")
    private String GOOGLE_API_KEY;

    @Override
    public EstimatedRoute calculateEstimatedRoute(String from, String to) throws InterruptedException, ApiException, IOException, RequestedRoutesNotFound {

        DirectionsRequest directionsRequest = new DirectionsRequest(from, to, null);

        //TODO: find out why there is no status field
        DirectionsResult result = getDirectionsResult(directionsRequest);

        if (result.routes.length < 1) {
            throw new RequestedRoutesNotFound("Routes for requested directions were not found.", directionsRequest.getOrigin(), directionsRequest.getDestination());
        }

        //TODO: implement searching for the best option
        return calculateRoute(result);
    }

    public EstimatedRoute calculateEstimatedRoute(DirectionsRequest directionsRequest) throws InterruptedException, ApiException, IOException, RequestedRoutesNotFound {
        //TODO: find out why there is no status field
        DirectionsResult result = getDirectionsResult(directionsRequest);

        if (result.routes.length < 1) {
            throw new RequestedRoutesNotFound("Routes for requested directions were not found.", directionsRequest.getOrigin(), directionsRequest.getDestination());
        }

        //TODO: implement searching for the best option
        return calculateRoute(result);
    }

    @Override
    public Ride calculateBestRoute(Ride ride, String[] waypoints) throws InterruptedException, RequestedRoutesNotFound, ApiException, IOException {
        return ride;
    }

    /**
     * Method to request directions from Directions API
     * @param request object containing necessary params
     * @return
     * @throws InterruptedException
     * @throws ApiException
     * @throws IOException
     */
    private DirectionsResult getDirectionsResult(DirectionsRequest request) throws InterruptedException, ApiException, IOException {
        DirectionsApiRequest apiRequest = DirectionsApi.getDirections(geoApiContext(), request.getOrigin(), request.getDestination());
        if (!validateWaypoints(request.getWaypoints()))
            return apiRequest.units(Unit.IMPERIAL).await();
        else
            return apiRequest.units(Unit.IMPERIAL).waypoints(prepareWaypoints(request.getWaypoints())).optimizeWaypoints(true).await();
    }

    private String[] prepareWaypoints(String[] waypoints){
        return Arrays.stream(waypoints)
                .filter(s -> (s != null && s.length() > 0))
                .toArray(String[]::new);
    }

    private boolean validateWaypoints(String[] waypoints){
        if (waypoints == null)
            return false;
        for (String s: waypoints){
            if (s != null)
                return true;
        }
        return false;
    }

    /**
     * Method destination calculate estimated route total distance and duration
     * origin Directions API result
     * @param directionsResult - Directions API request result
     * @return
     */
    private EstimatedRoute calculateRoute(DirectionsResult directionsResult){
        EstimatedRoute route = new EstimatedRoute();
        Arrays.stream(directionsResult.routes[0].legs).forEach(l -> {
            route.addDistance(l.distance.inMeters);
            route.addDuration(l.duration.inSeconds);
        });
        return route;
    }

    @Bean
    @Scope("singleton")
    public GeoApiContext geoApiContext(){
        return new GeoApiContext.Builder().apiKey(GOOGLE_API_KEY).build();
    }
}

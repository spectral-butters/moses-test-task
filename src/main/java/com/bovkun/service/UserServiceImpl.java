package com.bovkun.service;

import com.bovkun.controller.dto.Roles;
import com.bovkun.controller.dto.UserRegistrationDto;
import com.bovkun.database.entities.Role;
import com.bovkun.database.entities.User;
import com.bovkun.database.repository.RoleRepository;
import com.bovkun.database.repository.UserRepository;
import com.bovkun.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public User create(UserRegistrationDto userRegistrationDto) {
        User user = new User();
        user.setUsername(userRegistrationDto.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));
        Optional<Role> optRole = roleRepository.findById(Roles.USER.getId());
        optRole.ifPresent(role -> user.setRoles(Collections.singletonList(role)));
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> userOpt = userRepository.findOneByUsername(username);
        if (userOpt.isPresent())
            return userOpt.get();
        return null;
    }

    @Override
    public User getUserFromToken(Object principal) {
        org.springframework.security.core.userdetails.User securityUser = (org.springframework.security.core.userdetails.User) principal;
        return findByUsername(securityUser.getUsername());
    }
}

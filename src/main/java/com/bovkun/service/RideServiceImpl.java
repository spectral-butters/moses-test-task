package com.bovkun.service;

import com.bovkun.controller.dto.RideRequestDto;
import com.bovkun.controller.dto.VehicleTypes;
import com.bovkun.controller.dto.google.DirectionsRequest;
import com.bovkun.controller.dto.google.EstimatedRoute;
import com.bovkun.database.entities.*;
import com.bovkun.database.repository.*;
import com.bovkun.exception.RequestedRoutesNotFound;
import com.bovkun.exception.NoAvailableVehiclesException;
import com.bovkun.exception.RestrictedAccessException;
import com.bovkun.exception.SettingsNotFoundException;
import com.bovkun.service.interfaces.GoogleMapsService;
import com.bovkun.service.interfaces.RideService;
import com.google.maps.errors.ApiException;
import javassist.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class RideServiceImpl implements RideService {

    private final RideRepository rideRepository;
    private final RideRequestRepository rideRequestRepository;
    private final VehicleRepository vehicleRepository;
    private final SettingsRepository settingsRepository;
    private final UserRepository userRepository;

    private final GoogleMapsService googleMapsService;

    @Autowired
    public RideServiceImpl(RideRepository rideRepository, RideRequestRepository rideRequestRepository, VehicleRepository vehicleRepository, SettingsRepository settingsRepository, UserRepository userRepository, GoogleMapsService googleMapsService) {
        this.rideRepository = rideRepository;
        this.rideRequestRepository = rideRequestRepository;
        this.vehicleRepository = vehicleRepository;
        this.settingsRepository = settingsRepository;
        this.userRepository = userRepository;
        this.googleMapsService = googleMapsService;
    }

    @Override
    public Ride createOrUpdateRide(Ride ride) {
        return rideRepository.save(ride);
    }

    @Override
    public List<Ride> findRidesByRideRequest(RideRequest rideRequest) {
        return null;
    }

    @Override
    public List<Ride> findRidesByOriginAndPassengers(String origin, Integer passengers) {
        return rideRepository.findRidesByOriginAndPassengersAndStatus(origin, passengers, RideStatus.CREATED);
    }

    @Override
    public Ride processRideRequest(RideRequestDto rideRequestDto) throws InterruptedException, ApiException, RequestedRoutesNotFound, NoAvailableVehiclesException, IOException, SettingsNotFoundException {
        List<Ride> rides = rideRepository.findRidesByOriginAndPassengersAndStatus(rideRequestDto.getOrigin(), rideRequestDto.getPassengersAmount(), RideStatus.CREATED);
        RideRequest rideRequest = fromRideRequestDtoToRideRequest(rideRequestDto);

        if (!rides.isEmpty()){
            Ride ride = findAppropriateRide(rides, rideRequestDto, rideRequest);
            if (ride != null) {
                rideRequest.setRide(ride);
                rideRequestRepository.save(rideRequest);
                ride.getRideRequests().add(rideRequest);
                ride.setPassengersAmount(ride.getPassengersAmount() + rideRequestDto.getPassengersAmount());

                Ride returnRide;
                if (ride.getVehicle().getType().getSeats().compareTo(ride.getPassengersAmount()) == 0){
                    returnRide = finishRide(ride);
                    deductUserBalances(returnRide);
                } else
                    returnRide = createOrUpdateRide(ride);

                return returnRide;
            }
        }

        //rideRequestRepository.save(rideRequest);
        Ride newRide = fromRideRequestToRide(rideRequest, rideRequestDto.getVehicleType());
        return createRide(newRide);
    }

    @Override
    public Ride startRide(Long rideId, User user) throws NotFoundException, SettingsNotFoundException, RestrictedAccessException {
        Optional<Ride> rideOpt = rideRepository.findById(rideId);
        if (!rideOpt.isPresent())
            throw new NotFoundException("Requested ride was not found.");
        Ride ride = rideOpt.get();
        if (!user.getId().equals(ride.getUser().getId()))
            throw new RestrictedAccessException("Wrong ride owner for ride " + ride.getId() + ". Ride will not be started.");
        return finishRide(ride);
    }

    /**
     * Assuming immediate finish
     * @param ride
     * @return
     */
    private Ride finishRide(Ride ride) throws SettingsNotFoundException {
        ride.setStatus(RideStatus.FINISHED);
        ride.getVehicle().setAvailable(true);
        ride.setPrice(calculateRidePrice(ride));
        vehicleRepository.save(ride.getVehicle());
        deductUserBalances(ride);
        return createOrUpdateRide(ride);
    }

    /**
     * Method to calculate price for ride
     * @param ride
     * @return
     */
    private Double calculateRidePrice(Ride ride) throws SettingsNotFoundException {
        Optional<Setting> perMilePriceOpt = settingsRepository.findByKey(Constants.PER_MILE_PRICE_SETTING);
        Optional<Setting> perMinPriceOpt = settingsRepository.findByKey(Constants.PER_MINUTE_PRICE_SETTING);
        Double perMilePrice = null;
        Double perMinPrice = null;
        if (!perMilePriceOpt.isPresent())
            throw new SettingsNotFoundException("No settings available for price calculation.", Constants.PER_MILE_PRICE_SETTING);
        if (!perMinPriceOpt.isPresent())
            throw new SettingsNotFoundException("No settings available for price calculation.", Constants.PER_MINUTE_PRICE_SETTING);

        perMilePrice = Double.valueOf(perMilePriceOpt.get().getValue());
        perMinPrice = Double.valueOf(perMinPriceOpt.get().getValue());

        Double distancePrice = ride.getDistance() / 1000 * perMilePrice;
        Double durationPrice = ride.getDuration() / 60 * perMinPrice;
        return (distancePrice + durationPrice) * ride.getVehicle().getType().getPriceMultiplier();
    }

    /**
     * Calculate total price and deduct user balances
     * @param ride
     */
    private void deductUserBalances(Ride ride){
        Integer passengersAmount = ride.getPassengersAmount();
        ride.getRideRequests().forEach(rideRequest -> {
            User u = rideRequest.getUser();
            Double userPrice = (double) (ride.getPrice() / ride.getPassengersAmount() * rideRequest.getPassengersAmount());
            u.setBalance(u.getBalance() - userPrice);
            userRepository.save(u);
        });
    }

    /**
     * Method to create new ride
     * @param ride
     * @return
     */
    private Ride createRide(Ride ride){
        ride.getVehicle().setAvailable(false);
        vehicleRepository.save(ride.getVehicle());
        return createOrUpdateRide(ride);
    }

    /**
     * Method to convert rideRequestDtoObject into rideRequest entity
     * @param rideRequestDto
     * @return
     */
    private RideRequest fromRideRequestDtoToRideRequest(RideRequestDto rideRequestDto){
        RideRequest rideRequest = new RideRequest();
        rideRequest.setOrigin(rideRequestDto.getOrigin());
        rideRequest.setDestination(rideRequestDto.getDestination());
        rideRequest.setUser(rideRequestDto.getUser());
        rideRequest.setPassengersAmount(rideRequestDto.getPassengersAmount());
        return rideRequest;
    }

    /**
     * Method to create a new ride out of ride request
     * @param rideRequest
     * @param vehicleType
     * @return
     * @throws NoAvailableVehiclesException
     * @throws InterruptedException
     * @throws RequestedRoutesNotFound
     * @throws ApiException
     * @throws IOException
     */
    private Ride fromRideRequestToRide(RideRequest rideRequest, Integer vehicleType) throws NoAvailableVehiclesException, InterruptedException, RequestedRoutesNotFound, ApiException, IOException {
        Ride ride = new Ride();
        //TODO: add type selection
        Vehicle vehicle = null;
        if (vehicleType != null){
            List<Vehicle> vehicles = vehicleRepository.findAvailableVehicleByType(vehicleType);
            if (!vehicles.isEmpty())
                vehicle = vehicles.get(0);
        }
        else {
            List<Vehicle> vehicles = vehicleRepository.findOneByAvailableIsTrue();
            if (!vehicles.isEmpty())
                vehicle = vehicles.get(0);
        }

        if (vehicle == null)
            throw new NoAvailableVehiclesException("No available vehicles of type " + vehicleType + " were found.");

        ride.setStatus(RideStatus.CREATED);
        ride.setPassengersAmount(rideRequest.getPassengersAmount());
        ride.setOrigin(rideRequest.getOrigin());
        ride.setDestination(rideRequest.getDestination());
        EstimatedRoute estimatedRoute = googleMapsService.calculateEstimatedRoute(rideRequest.getOrigin(), rideRequest.getDestination());
        ride.setDistance(estimatedRoute.getDistance());
        ride.setDuration(estimatedRoute.getDuration());
        ride.setUser(rideRequest.getUser());
        ride.getRideRequests().add(rideRequest);
        ride.setVehicle(vehicle);
        return ride;
    }

    /**
     * Search for the best possible ride route
     * @param ride
     * @return
     */
    private Ride findBestRoute(Ride ride) {
        //TODO: implement best Route search algo
        return null;
        //String[] waypoints = getRideWaypoints(ride);
        //return googleMapsService.calculateBestRoute(ride, waypoints);
    }

    /**
     * Find suitable ride according to ride request and our setting
     * @param rides rides, which have
     * @param rideRequestDto
     * @return first appropriate ride or null
     */
    private Ride findAppropriateRide(List<Ride> rides, RideRequestDto rideRequestDto, RideRequest rideRequest) throws InterruptedException, ApiException, RequestedRoutesNotFound, IOException {
        //TODO: price optimization algo
        Ride bestRide = null;
        EstimatedRoute tmp = new EstimatedRoute();
        EstimatedRoute withoutRide = googleMapsService.calculateEstimatedRoute(rideRequestDto.getOrigin(), rideRequestDto.getDestination());
        long timeDelay = getTimeDelay();
        for (Ride ride : rides) {
            String[] waypoints = getRideWaypoints(ride);
            if (!mergeDestinations(ride, waypoints, rideRequestDto.getDestination())){
                waypoints[waypoints.length - 1] = rideRequestDto.getDestination();
                ride.getWaypoints().add(new Waypoint(rideRequestDto.getDestination()));
            } else {
                return ride;
            }
            DirectionsRequest directionsRequest = new DirectionsRequest(ride.getOrigin(), ride.getDestination(), waypoints);
            EstimatedRoute estimatedRoute = googleMapsService.calculateEstimatedRoute(directionsRequest);
            if (tmp.getDistance() == 0){
                tmp = estimatedRoute;
            }
            if (validateEstimatedRouteDuration(ride, estimatedRoute, withoutRide, timeDelay)){
                if (bestRide == null){
                    bestRide = ride;
                    bestRide.setDuration(estimatedRoute.getDuration());
                    bestRide.setDistance(estimatedRoute.getDistance());
                    continue;
                }
                bestRide = estimatedRoute.getDuration() < tmp.getDuration() ? ride : bestRide;
            }
        }
        return bestRide;
    }

    /**
     * Method to search for close distances between ride destination or waypoints and new destination
     * If found - we won't add another destination
     * @param ride ride
     * @param waypoints ride's current waypoints
     * @param destination new destination
     * @return
     */
    private boolean mergeDestinations(Ride ride, String[] waypoints, String destination) throws InterruptedException, RequestedRoutesNotFound, ApiException, IOException {
        EstimatedRoute estimatedRoute = googleMapsService.calculateEstimatedRoute(ride.getDestination(), destination);
        if (estimatedRoute.getDistance() <= Constants.DEFAULT_DISTANCE_TO_MERGE)
            return true;
        for (String s: waypoints){
            if (s == null)
                continue;
            estimatedRoute = googleMapsService.calculateEstimatedRoute(s, destination);
            if (estimatedRoute.getDistance() <= Constants.DEFAULT_DISTANCE_TO_MERGE)
                return true;
        }
        return false;
    }

    /**
     * Method to add waypoints from Set to String array
     * @param ride
     * @return
     */
    private String[] getRideWaypoints(Ride ride){
        int waypointCounter = 0;
        String[] waypoints = new String[ride.getWaypoints().size() + 1];
        for (Waypoint waypoint : ride.getWaypoints())
            waypoints[waypointCounter++] = waypoint.getValue();
        return waypoints;
    }

    /**
     * Read time delay from database or return default value
     * @return
     */
    private long getTimeDelay(){
        Optional<Setting> settingOptional = settingsRepository.findByKey(Constants.TIME_DELAY_SETTING);
        if (settingOptional.isPresent()){
            try {
              return Long.valueOf(settingOptional.get().getValue());
            } catch (NumberFormatException e){
                log.error(e.getMessage());
                return Constants.DEFAULT_TIME_DELAY;
            }
        }
        return Constants.DEFAULT_TIME_DELAY;
    }

    /**
     * Validate if new time will be suitable for a customer
     * @param ride
     * @param route
     * @return
     */
    private boolean validateEstimatedRouteDuration(Ride ride, EstimatedRoute route, EstimatedRoute withoutRide, long timeDelay){
        boolean valid = true;
        long currentDuration = ride.getDuration()/60;
        if (currentDuration > 60)
            return valid;
        valid = route.getDurationInMinutes() - currentDuration <= timeDelay &&
                route.getDurationInMinutes() - withoutRide.getDurationInMinutes() <= timeDelay;;
        return valid;
    }
}

package com.bovkun.service.interfaces;

import com.bovkun.controller.dto.RideRequestDto;
import com.bovkun.database.entities.Ride;
import com.bovkun.database.entities.RideRequest;
import com.bovkun.database.entities.User;
import com.bovkun.exception.RequestedRoutesNotFound;
import com.bovkun.exception.NoAvailableVehiclesException;
import com.bovkun.exception.RestrictedAccessException;
import com.bovkun.exception.SettingsNotFoundException;
import com.google.maps.errors.ApiException;
import javassist.NotFoundException;

import java.io.IOException;
import java.util.List;

public interface RideService {
    Ride createOrUpdateRide(Ride ride);
    List<Ride> findRidesByRideRequest(RideRequest rideRequest);
    List<Ride> findRidesByOriginAndPassengers(String origin, Integer passengers);
    Ride processRideRequest(RideRequestDto rideRequestDto) throws InterruptedException, ApiException, RequestedRoutesNotFound, NoAvailableVehiclesException, IOException, SettingsNotFoundException;
    Ride startRide(Long rideId, User user) throws NotFoundException, SettingsNotFoundException, RestrictedAccessException;
}

package com.bovkun.service.interfaces;

import com.bovkun.controller.dto.UserRegistrationDto;
import com.bovkun.database.entities.User;

public interface UserService {
    User create(UserRegistrationDto user);
    User findByUsername(String username);
    User getUserFromToken(Object principal);
}

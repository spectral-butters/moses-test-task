package com.bovkun.service.interfaces;

import com.bovkun.database.entities.Setting;
import com.bovkun.database.entities.User;
import com.bovkun.database.entities.VehicleType;
import javassist.NotFoundException;

public interface AdminService {
    Setting updatePerMilePrice(Double value) throws NotFoundException;
    Setting updatePerMinutePrice(Double value) throws NotFoundException;
    User createAdmin(Long userId) throws NotFoundException;
    User updateBalance(Long userId, Double balance) throws NotFoundException;
    VehicleType updateVehicleType(Integer type, Double priceMult) throws NotFoundException;
}

package com.bovkun.service.interfaces;

import com.bovkun.controller.dto.google.DirectionsRequest;
import com.bovkun.controller.dto.google.EstimatedRoute;
import com.bovkun.database.entities.Ride;
import com.bovkun.exception.RequestedRoutesNotFound;
import com.google.maps.errors.ApiException;

import java.io.IOException;

public interface GoogleMapsService {
    EstimatedRoute calculateEstimatedRoute(String from, String to) throws InterruptedException, ApiException, IOException, RequestedRoutesNotFound;
    EstimatedRoute calculateEstimatedRoute(DirectionsRequest request) throws InterruptedException, ApiException, IOException, RequestedRoutesNotFound;

    Ride calculateBestRoute(Ride ride, String[] waypoints) throws InterruptedException, RequestedRoutesNotFound, ApiException, IOException;
}

package com.bovkun.security;

public interface SecurityConstants {
    String AUTHORIZATION_HEADER_STRING = "Authorization";
    String TOKEN_PREFIX = "Bearer ";
    String SECRET = "s3#RD#D!#FSA";
    Integer TOKEN_VALIDITY_TIME = 1;
}

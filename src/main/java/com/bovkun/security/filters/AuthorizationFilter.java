package com.bovkun.security.filters;

import com.bovkun.database.entities.User;
import com.bovkun.security.SecurityConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class AuthorizationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public AuthorizationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        User user = new User();
        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        ZonedDateTime expirationUTC = ZonedDateTime.now(ZoneOffset.UTC).plus(SecurityConstants.TOKEN_VALIDITY_TIME, ChronoUnit.HOURS);
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authResult.getPrincipal();

        String token = Jwts.builder().setSubject(user.getUsername())
                .setExpiration(Date.from(expirationUTC.toInstant()))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET).compact();

        response.addHeader("Content-Type", "application/json");
        response.getWriter().write(getAuthTokenJson(SecurityConstants.TOKEN_PREFIX + token));
    }

    private String getAuthTokenJson(String token) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode authToken = mapper.createObjectNode();
        authToken.put("Auth-Token", token);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(authToken);
    }
}

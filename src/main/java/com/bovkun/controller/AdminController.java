package com.bovkun.controller;

import com.bovkun.controller.dto.SettingDto;
import com.bovkun.controller.dto.VehicleTypeDto;
import com.bovkun.service.interfaces.AdminService;
import javassist.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping("/admin")
public class AdminController {
    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/setting/price_per_minute")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updatePricePerMinute(@RequestBody SettingDto settingDto){
        Double value = null;
        try {
            value = Double.valueOf(settingDto.getValue());
        } catch (Exception e) {
            log.info(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        try {
            adminService.updatePerMinutePrice(value);
        } catch (NotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/setting/price_per_mile")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updatePricePerMile(@RequestBody SettingDto settingDto){
        Double value = null;
        try {
            value = Double.valueOf(settingDto.getValue());
        } catch (Exception e) {
            log.info(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        try {
            adminService.updatePerMilePrice(value);
        } catch (NotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("/vehicletype")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity updateVehicleType(@RequestBody VehicleTypeDto vehicleTypeDto){
        try {
            adminService.updateVehicleType(vehicleTypeDto.getType(), vehicleTypeDto.getPriceMultiplier());
        } catch (NotFoundException e) {
            log.info(e.getMessage());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.ok().build();
    }



}

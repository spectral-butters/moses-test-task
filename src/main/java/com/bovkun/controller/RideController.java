package com.bovkun.controller;

import com.bovkun.controller.dto.RideDto;
import com.bovkun.controller.dto.RideRequestDto;
import com.bovkun.database.entities.Ride;
import com.bovkun.database.entities.User;
import com.bovkun.exception.NoAvailableVehiclesException;
import com.bovkun.exception.RequestedRoutesNotFound;
import com.bovkun.exception.RestrictedAccessException;
import com.bovkun.exception.SettingsNotFoundException;
import com.bovkun.service.interfaces.RideService;
import com.bovkun.service.interfaces.UserService;
import com.google.maps.errors.ApiException;
import javassist.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/ride")
@Log4j2
public class RideController {

    private final RideService rideService;
    private final UserService userService;

    @Autowired
    public RideController(RideService rideService, UserService userService) {
        this.rideService = rideService;
        this.userService = userService;
    }

    @GetMapping
    public List<RideDto> getRidesList(@RequestParam("origin") String origin, @RequestParam("passengers") Integer passengers){
        List<Ride> rides = rideService.findRidesByOriginAndPassengers(origin, passengers);
        List<RideDto> rideDtos = new ArrayList<>();
        for (Ride ride : rides)
            rideDtos.add(new RideDto(ride));
        return rideDtos;
    }

    @PostMapping
    public ResponseEntity<RideDto> requestRide(@RequestBody RideRequestDto rideRequestDto) {
        User user = userService.getUserFromToken(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        rideRequestDto.setUser(user);
        Ride ride = null;
        try {
            ride = rideService.processRideRequest(rideRequestDto);

        } catch (NoAvailableVehiclesException nav) {
            log.debug(nav.getMessage());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

        } catch (SettingsNotFoundException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        } catch (ApiException | InterruptedException | IOException e ) {
            log.error("Error occur while requesting Google API.\n"+e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();

        } catch (RequestedRoutesNotFound requestedRoutesNotFound) {
            log.info(requestedRoutesNotFound.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        if (ride == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(new RideDto(ride));
    }

    @PostMapping("/start")
    public ResponseEntity<RideDto> startRide(@RequestBody RideDto rideDto){
        User user = userService.getUserFromToken(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        Ride ride;
        try {
            ride = rideService.startRide(rideDto.getId(), user);
        } catch (RestrictedAccessException e) {
            log.info(e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).build();
        } catch (SettingsNotFoundException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (NotFoundException e) {
            log.debug(e.getMessage());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        if (ride == null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        return ResponseEntity.ok(new RideDto(ride));
    }

}

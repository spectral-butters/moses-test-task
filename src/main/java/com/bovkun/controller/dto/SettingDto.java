package com.bovkun.controller.dto;

import lombok.Data;

@Data
public class SettingDto {
    private String key;
    private String value;
}

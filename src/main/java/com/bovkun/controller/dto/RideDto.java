package com.bovkun.controller.dto;

import com.bovkun.database.entities.Ride;
import com.bovkun.database.entities.Waypoint;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@ToString
@NoArgsConstructor
public class RideDto {
    private Long id;
    private Long duration;
    private Long distance;
    private Set<Waypoint> waypoints;
    private String origin;
    private String destination;
    private Double price;

    public RideDto(Ride ride) {
        this.id = ride.getId();
        this.duration = ride.getDuration();
        this.distance = ride.getDistance();
        this.origin = ride.getOrigin();
        this.destination = ride.getDestination();
        this.price = ride.getPrice();
        waypoints = ride.getWaypoints();
    }
}

package com.bovkun.controller.dto;

import com.bovkun.database.entities.User;
import com.bovkun.database.entities.VehicleType;
import lombok.Data;
import lombok.ToString;
import org.springframework.lang.Nullable;

import java.util.Date;

@Data
@ToString
public class RideRequestDto {
    private User user;
    private String origin;
    private String destination;
    private Integer vehicleType;
    private Integer passengersAmount;
    //private Date startingAt;
}

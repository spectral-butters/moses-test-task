package com.bovkun.controller.dto.google;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class EstimatedRoute {
    // In meters
    private long distance;
    // In seconds
    private long duration;

    public EstimatedRoute(long distance, long duration) {
        this.distance = distance;
        this.duration = duration;
    }

    public long getDurationInMinutes(){
        return duration/60;
    }

    public void addDuration(long duration){
        this.duration += duration;
    }

    public void addDistance(long distance){
        this.distance += distance;
    }
}

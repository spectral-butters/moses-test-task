package com.bovkun.controller.dto.google;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DirectionsRequest {
    private String origin;
    private String destination;
    private String[] waypoints;

    public DirectionsRequest(String origin, String destination, String[] waypoints) {
        this.origin = origin;
        this.destination = destination;
        this.waypoints = waypoints;
    }
}

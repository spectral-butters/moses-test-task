package com.bovkun.controller.dto;

import lombok.Data;

@Data
public class VehicleTypeDto {
    private Integer type;
    private Double priceMultiplier;
}

package com.bovkun.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VehicleTypes {
    ECONOMY(1), BUS(2), BUSINESS(3);

    private final int code;
}

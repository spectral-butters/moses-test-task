package com.bovkun.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Roles {
    ADMIN(1), USER(2);

    private final int id;
}

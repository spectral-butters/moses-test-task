package com.bovkun.controller.dto;

import com.bovkun.database.entities.User;
import lombok.Data;

@Data
public class UserRegistrationDto {
    private String username;
    private String password;
    private String confirmPassword;
}

package com.bovkun.controller.dto;

import com.bovkun.database.entities.User;
import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String username;
    private Double balance;

    public UserDto(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.balance = user.getBalance();
    }
}

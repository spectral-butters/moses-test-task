package com.bovkun.controller;

import com.bovkun.controller.dto.UserDto;
import com.bovkun.controller.dto.UserRegistrationDto;
import com.bovkun.database.entities.User;
import com.bovkun.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<Void> createUser(@RequestBody UserRegistrationDto user){
        userService.create(user);
        SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/user")
    public ResponseEntity getUser(){
        User user = userService.getUserFromToken(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (user != null)
            return ResponseEntity.ok(new UserDto(user));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}

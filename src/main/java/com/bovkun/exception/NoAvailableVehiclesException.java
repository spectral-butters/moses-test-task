package com.bovkun.exception;

import com.bovkun.controller.dto.VehicleTypes;
import lombok.Getter;

/**
 * Exception which is thrown when a vehicle was not found
 */
@Getter
public class NoAvailableVehiclesException extends Exception {

    public NoAvailableVehiclesException(String message) {
        super(message);
    }

    public NoAvailableVehiclesException(String message, VehicleTypes vehicleType) {
        super(message + " Vehicle type " + vehicleType.name());
    }

    public NoAvailableVehiclesException(String message, Throwable cause) {
        super(message, cause);
    }
}

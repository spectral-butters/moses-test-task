package com.bovkun.exception;

public class RequestedRoutesNotFound extends Exception {
    public RequestedRoutesNotFound(String message) {
        super(message);
    }

    public RequestedRoutesNotFound(String message, String first, String second) {
        super(message + "First destination: " + first + ". Second destination " + second);
    }

    public RequestedRoutesNotFound(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.bovkun.exception;

public class SettingsNotFoundException extends Exception {

    public SettingsNotFoundException(String message, String key) {
        super(message + "\nKey not found: "+ key);
    }

    public SettingsNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

package com.bovkun.exception;

public class RestrictedAccessException extends Exception {

    public RestrictedAccessException(String s) {
        super(s);
    }
}

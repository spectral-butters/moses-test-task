package com.bovkun.database.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@EqualsAndHashCode(of = {"id", "user", "ride", "origin", "destination"})
public class RideRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "ride_id", referencedColumnName = "id")
    private Ride ride;

    @NotNull
    @Column(nullable = false)
    private Integer passengersAmount;

    @NotNull
    @Column(nullable = false)
    private String origin;

    @NotNull
    @Column(nullable = false)
    private String destination;

}

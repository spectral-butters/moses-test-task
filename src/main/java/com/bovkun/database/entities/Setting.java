package com.bovkun.database.entities;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Data
@Entity(name = "settings")
@NoArgsConstructor
public class Setting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String key;
    @Column(nullable = false)
    private String value;

    public Setting(String key, String value) {
        this.key = key;
        this.value = value;
    }
}

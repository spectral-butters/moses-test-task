package com.bovkun.database.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RideStatus {
    CREATED(1), FAILED(2), READY(4), FINISHED(3);

    private final int code;
}

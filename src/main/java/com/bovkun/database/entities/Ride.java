package com.bovkun.database.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(of = {"id", "user", "origin", "destination"})
public class Ride {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    private Double price;

    @Column
    private String origin;

    @Column String destination;

    @Column
    @Enumerated
    private RideStatus status;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @Column
    private Integer passengersAmount;

    @Column
    private Long distance;

    @Column
    private Long duration;

    @OneToMany(mappedBy = "ride", cascade = CascadeType.ALL)
    private Set<RideRequest> rideRequests = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ride_waypoint",
            joinColumns = @JoinColumn(name = "ride_id"),
            inverseJoinColumns = @JoinColumn(name = "waypoint_id")
    )
    @Column(nullable = false)
    private Set<Waypoint> waypoints = new HashSet<>();

}

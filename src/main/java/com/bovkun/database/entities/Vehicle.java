package com.bovkun.database.entities;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String plate;

    @NotNull
    @Column(nullable = false)
    @ColumnDefault("true")
    private Boolean available;

    @NotNull
    @ManyToOne
    private VehicleType type;

    @OneToMany(mappedBy = "vehicle")
    private List<Ride> rides;
}

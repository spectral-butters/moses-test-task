package com.bovkun.database.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode(of = {"id", "value"})
@Data
@NoArgsConstructor
public class Waypoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String value;

    @JsonIgnore
    @ManyToMany(mappedBy = "waypoints")
    private List<Ride> ride;

    public Waypoint(String value) {
        this.value = value;
    }
}

package com.bovkun.database.repository;

public interface Queries {
    String findRidesByOriginAndPassengersAndStatus = "SELECT r from Ride r join Vehicle v on r.vehicle = v.id join VehicleType vt on v.type = vt.id where r.origin=:origin AND vt.seats >= r.passengersAmount + :passengersAmount AND r.status = :status";
    String findAvailableVehicleByType = "select v from Vehicle v join VehicleType vt on v.type = vt.id where v.available = true and vt.id = :type";
}

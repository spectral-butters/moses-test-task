package com.bovkun.database.repository;

import com.bovkun.database.entities.Setting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettingsRepository extends JpaRepository<Setting, Long> {
    Optional<Setting> findByKey(String key);
    Setting save(Setting setting);
}

package com.bovkun.database.repository;

import com.bovkun.database.entities.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VehicleTypeRepository extends JpaRepository<VehicleType, Integer> {
    VehicleType save(VehicleType vehicleType);

    Optional<VehicleType> findById(Integer id);
}

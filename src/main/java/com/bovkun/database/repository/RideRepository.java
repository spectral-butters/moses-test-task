package com.bovkun.database.repository;

import com.bovkun.database.entities.Ride;
import com.bovkun.database.entities.RideStatus;
import com.bovkun.database.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RideRepository extends JpaRepository<Ride, Long> {
    Ride save(Ride ride);
    Optional<Ride> findById(Long rideId);
    @Query(value = Queries.findRidesByOriginAndPassengersAndStatus)
    List<Ride> findRidesByOriginAndPassengersAndStatus(@Param("origin") String origin, @Param("passengersAmount") Integer passengers, @Param("status") RideStatus status);
}

package com.bovkun.database.repository;

import com.bovkun.database.entities.RideRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RideRequestRepository extends JpaRepository<RideRequest, Long> {
    RideRequest save(RideRequest rideRequest);
    Optional<RideRequest> findOneById(Long id);
}

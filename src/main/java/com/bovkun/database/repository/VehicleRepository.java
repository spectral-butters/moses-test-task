package com.bovkun.database.repository;

import com.bovkun.database.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    @Query(value = Queries.findAvailableVehicleByType)
    List<Vehicle> findAvailableVehicleByType(@Param("type") Integer type);
    List<Vehicle> findOneByAvailableIsTrue();
    Vehicle save(Vehicle vehicle);
}
